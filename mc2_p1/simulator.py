import pandas as pd
import numpy as np

from portfolio import util, analysis
import datetime as dt


def compute_portvals(start_date, end_date, ordersfile, startval):
    # do stuff

    dates = pd.date_range(start_date, end_date)
    trade_mat = get_trade_matrix(ordersfile, dates)
    holding_mat = trade_mat.multiply(-1).cumsum()
    # print(holding_mat)

    prices = util.get_data(trade_mat.columns.tolist(), dates, addSPY=False)
    amounts = (trade_mat*prices).dropna()

    holdings = (holding_mat*prices).dropna()
    holdings['Value'] = holdings.sum(axis=1)

    amounts['Cash'] = amounts.sum(axis=1).cumsum() + startval

    df_portvals = amounts['Cash']+holdings['Value']
    df_portvals.columns = 'Value'


    return df_portvals

def get_trade_matrix(ordersfile, dates):
    dates = pd.DataFrame(dates, columns=['Date'])
    dates = dates.set_index(['Date'])
    # dates.name = 'Date'

    orders = pd.read_csv(ordersfile, parse_dates=[[0, 1, 2]], header=None,
                         usecols=[0, 1, 2, 3, 4, 5])

    orders.columns = ['Date', 'Symbol', 'Action', 'Amount']
    orders = orders.set_index(['Date'])

    orders.loc[:, ['Amount']] *= orders.Action.str.contains("Buy").multiply(-2).add(1)

    symbol_columns = np.unique(orders.Symbol.values)

    sym_dfs = []
    for col in symbol_columns:
        new_df = orders[orders.Symbol == col][['Amount']]
        new_df.columns = [col]

        sym_dfs.append(new_df)


    trade_matrix = pd.concat(sym_dfs, axis=0, join='outer').fillna(0)

    trade_matrix = trade_matrix.groupby(trade_matrix.index).sum()

    trade_matrix = trade_matrix.join(dates, how='right').fillna(0)

    return trade_matrix


def run_sim():

    df_portvals = compute_portvals('2011-01-10', '2011-12-20', '/Users/Jeff/ownCloud/BitBucket/School/Python/ml4t/mc2_p1/Orders-files/orders.csv', 1000000)
    print(analysis.get_portfolio_stats(df_portvals))

if __name__ == "__main__":
    run_sim()